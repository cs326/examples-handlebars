
exports.examples = function (req, res) {
	res.render('examples', { title: 'Handlebars Examples' });
};

exports.example_01 = function (req, res){
  res.render('example_01', { title: 'Example 01' });
};

exports.example_02 = function (req, res){
  res.render('example_02', { title: 'Example 02' });
};

exports.example_03 = function (req, res){
  res.render('example_03', { title: 'Example 03' });
};

exports.example_04 = function (req, res){
  res.render('example_04', { title: 'Example 04' });
};

exports.example_05 = function (req, res){
  res.render('example_05', { title: 'Example 05' });
};