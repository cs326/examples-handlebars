

var express  = require('express'),
    examples = require('./routes/examples'),
    http     = require('http'),
    path     = require('path');

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('your secret here'));
  app.use(express.session());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'client')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/', examples.examples);
app.get('/example_01', examples.example_01);
app.get('/example_02', examples.example_02);
app.get('/example_03', examples.example_03);
app.get('/example_04', examples.example_04);
app.get('/example_05', examples.example_05);

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
