$(function () {

  var context = {
    users: [
        {firstname : 'John1', lastname: 'Doe1', admin: true},
        {firstname : 'Jane2', lastname: 'Doe2', admin: false},
        {firstname : 'John3', lastname: 'Doe3', admin: true},
        {firstname : 'Jane4', lastname: 'Doe4', admin: false},
        {firstname : 'John5', lastname: 'Doe5', admin: true},
        {firstname : 'Jane6', lastname: 'Doe6', admin: false},
        {firstname : 'John7', lastname: 'Doe7', admin: true},
        {firstname : 'Jane8', lastname: 'Doe8', admin: false},
        {firstname : 'John9', lastname: 'Doe9', admin: true},
        {firstname : 'Jane10', lastname: 'Doe10', admin: false},
        {firstname : 'John11', lastname: 'Doe11', admin: true},
        {firstname : 'Jane12', lastname: 'Doe12', admin: false}
      ]
    };

  var template = Handlebars.templates.users;
  var html     = template(context);

  $('div#view').append(html);

});